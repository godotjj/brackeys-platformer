extends Area2D

## It uses the [AnimationPlayer] to reproduce the coin sound and remove it from the scene.
## This is because if we `queue_free()` just after playing the sound, it won't play at all.

@onready var game_manager = %GameManager
@onready var animation_player = $AnimationPlayer

func _on_body_entered(body:Node2D) -> void:
	game_manager.add_point()
	animation_player.play('pickup')
